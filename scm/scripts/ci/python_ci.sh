#!/bin/bash

set -e

ROOT_DIR=$(git rev-parse --show-toplevel)
COMMON_DIR=${ROOT_DIR}/scm/scripts/common

source "${COMMON_DIR}/echo.sh"

cd "${ROOT_DIR}"
pwd

echo_func "[scm] Install the Python package"
REQS=$(find ./ -name "requirements.txt")
for REQ in ${REQS}
do
	echo_func "[scm] Try to install in ${REQ}.." 0
	python3 -m pip install -U -r "${REQ}"
done

# check to exist the python files
py_files=$(find . \
	-type f \
	-not -path '*/.git/*' -a \
	-not -path "${THIRD_PARTY_PATH}" -a \
	-not -path "${OUT_PATH}" -a \
	-not -path "${CGTOOLS_PATH}" -a \
	-not -path "${IQMATH_PATH}" -a \
	-not -path "${DOCS_PATH}" -a \
	-not -path "${PYTHON_ENV_PATH}" \
	-name '*.py')
if [[ "${py_files}" == "" ]]
then
	echo_func "[SCM ERR] Not exist the python files!" 1
	exit 0
fi

# check the coding style score
for py_file in ${py_files}
do
	echo_func "[scm] Start the checking coding style about the ${py_file}" 0

	# sort the import
	python3 -m isort "${py_file}"
	echo_func "[scm] Completed to check the \"isort\"" 0

	"${COMMON_DIR}/diff_check.sh"

	RET="$?"
	if [ "${RET}" -ne "0" ]
	then
		exit 1
	fi

	# coding style check
	python3 -m flake8 "${py_file}"

	RET="$?"
	if [ "${RET}" -ne "0" ]
	then
		exit 1
	fi
	echo_func "[scm] Completed to check the \"flake8\""

	lint_result=$(python3 -m pylint "${py_file}" 2>&1 | \
		grep "Your code" | \
		awk '{print $7}' | \
		awk -F '/' '{print $1}')
	if [[ "${lint_result}" != "10.00" ]]
	then
		echo_func "[SCM ERR] pylint result is ${lint_result}" 1
		python3 -m pylint "${py_file}"
		exit 1
	fi
	echo_func "[scm] Completed to check the \"pylint\""
	echo
done

echo_func "[scm] Python CI test done!" 0
