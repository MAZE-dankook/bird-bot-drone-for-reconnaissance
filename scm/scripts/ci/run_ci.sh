#!/bin/bash

SCRIPT_DIR="./scm/scripts"
CI_DIR="${SCRIPT_DIR}/ci"
ROOT_DIR=$(git rev-parse --show-toplevel)

set -e

source "${ROOT_DIR}/scm/scripts/common/echo.sh"

cd "${ROOT_DIR}"

# general
bash ${CI_DIR}/general_ci.sh

# lang check
bash ${CI_DIR}/bash_ci.sh
bash ${CI_DIR}/clang_ci.sh
bash ${CI_DIR}/cpp_ci.sh
bash ${CI_DIR}/python_ci.sh

# static analytics
#bash ${CI_DIR}/static_analytics.sh

echo_func "Done!" 0
