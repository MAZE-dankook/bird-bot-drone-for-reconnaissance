#!/bin/bash

set -e

ROOT_DIR=$(git rev-parse --show-toplevel)
COMMON_DIR=${ROOT_DIR}/scm/scripts/common
LINT_LOG="cmake_lint.log"

source "${COMMON_DIR}/echo.sh"

cd "${ROOT_DIR}"
pwd

# CI using the cmakelint
CMAKE_LISTS=$(find . \
	-not -path "${THIRD_PARTY_PATH}" -a \
	-not -path "${OUT_PATH}" -a \
	-not -path "${CGTOOLS_PATH}" -a \
	-not -path "${IQMATH_PATH}" -a \
	-not -path "${DOCS_PATH}" \
	-name "CMakeLists.txt" | tr '\n' ' ')
for LIST_FILE in ${CMAKE_LISTS}
do
	cmakelint --spaces=2 --linelength=80 "${LIST_FILE}" >> "${LINT_LOG}" 2>&1
done

echo_func "[scm] checklist below,," 0
echo "${CMAKE_LISTS}"
echo

# Check the error
ERRORS=$(grep "Total Errors" < ${LINT_LOG} | awk '{print $3}')
for NUM in ${ERRORS}
do
	if [[ "${NUM}" != "0" ]]
	then
		echo_func "[SCM ERR] Need to check the CMakeLists.txt" 1
		exit 1
	fi
done

rm ${LINT_LOG}

echo_func "[scm] CMakeLists.txt CI test done!" 0
