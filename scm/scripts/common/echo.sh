#!/bin/bash

BACK_CR="\033[0m"
RED_CR="\033[0;31m"
GREEN_CR="\033[0;32m"

export THIRD_PARTY_PATH="*/third_party/*"
export OUT_PATH="*/out/*"
export CGTOOLS_PATH="*/cgtools/*"
export IQMATH_PATH="*/IQmath/*"
export C28X_PATH="*/linux/core/*"
export DOCS_PATH="*/docs/*"
export PYTHON_ENV_PATH="*/.env/*"

echo_func()
{
	if [[ $2 -eq 1 ]]
	then
		echo -e "${RED_CR}${1}${BACK_CR}"
	else
		echo -e "${GREEN_CR}${1}${BACK_CR}"
	fi
}
