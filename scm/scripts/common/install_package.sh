#!/bin/bash

ROOT_DIR=$(git rev-parse --show-toplevel)
source "${ROOT_DIR}/scm/scripts/common/echo.sh"

SUDO=
if [[ "${UID}" == "1000" && "${EUID}" == "1000" ]]
then
	SUDO="sudo"
fi

echo_func "[scm] Delete unnecessary files"
${SUDO} pkill apt
${SUDO} pkill apt-get
${SUDO} rm /var/lib/apt/lists/lock
${SUDO} rm /var/cache/apt/archives/lock
${SUDO} rm /var/lib/dpkg/lock*
${SUDO} dpkg --configure -a

set -e

cd "${ROOT_DIR}"

echo_func "[scm] Update and Upgrade the package"
${SUDO} apt-get update && ${SUDO} apt-get upgrade -y

echo_func "[scm] Install the basic package"
${SUDO} apt-get install -y build-essential \
		software-properties-common \
		python3-pip \
		python3-dev \
		python3-setuptools \
		python3-venv \
		gcc \
		git \
		make \
		clang-format \
		valgrind \
		shellcheck \
		cppcheck \
		libc6-i386 \
		wget

# for Chrome
wget \
	-q \
	-O \
	- https://dl.google.com/linux/linux_signing_key.pub | \
	${SUDO} apt-key add -

${SUDO} sh -c \
	'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list'

${SUDO} apt update
${SUDO} apt install google-chrome-stable -y
${SUDO} rm /etc/apt/sources.list.d/google.list

echo_func "[scm] Install the Python package"
${SUDO} python3 -m pip install -U pip
REQS=$(find ./ -name "requirements.txt")
for REQ in ${REQS}
do
	echo_func "[scm] Try to install in ${REQ}.." 0
	${SUDO} python3 -m pip install -U -r "${REQ}"
done

${SUDO} apt-get autoremove -y
${SUDO} apt-get autoclean -y
${SUDO} apt-get clean
echo_func "[scm] Complete to update/install all packages!"
