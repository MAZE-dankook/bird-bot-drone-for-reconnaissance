# Information in Linux Directory


## Prerequisite

* Need to necessary packages for the Ubuntu(based on 20.04)

```bash
$ cd <ROOT directory>
$ bash scm/scripts/common/install_package.sh
```


## utils

* The utils directory includes the scripts that should be executed when before the core build.

* First, you need to execute the ***get_c28x_compiler.sh*** and also execute the ***get_c28x_IQmath_library.sh***.

```bash
$ cd utils
$ ./get_c28x_compiler.sh
$ ./get_c28x_IQmath_library.sh
$ cd - && tree -L 1
.
├── cgtools
├── core
├── IQmath
├── README.md
└── utils

4 directories, 1 file
```

* If the script runs successfully, you can see the **cgtools** and **IQmath** directories.


### get_c28x_compiler.sh

* Download the CCS SDK from TI Webpage, install the CCS and extract the **header** files, **library**, **compiler**, and **linker** for the build.


### get_c28x_IQmath_library.sh

* To download the **C2000 Ware**, One needs to access the login and accept the conditions of TI policy.

  * [IQmath Library Download Issue](https://gitlab.com/MAZE-dankook/bird-bot-drone-for-reconnaissance/-/issues/6)

* So, This script try to login the TI web page and automatically download the C2000WARE as the latest version.

* When executing the script, you can check the C2000 Ware installation file in **~/Downloads**



## core

* The firmware source codes are based on the skeleton code structure of TMS320F2808.

* Please refer to the README file below,

  * [core directory README](./core/README.md)


## cgtools

* This directory has basic **header** files, the rts28x **library** and the **compiler/linker** for C28x.

* The source codes in the **core** directory refer to that path for the build.


## IQmath

* This directory has header files and the library for using IQmath.

* Also, the source codes in **core** directory refer to that path for the build.
