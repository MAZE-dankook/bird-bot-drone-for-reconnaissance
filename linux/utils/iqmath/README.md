# Auto Download C2000WARE for IQmath


## Set the Envrionment

```bash
$ python3 -m venv .env
$ source .env/bin/activate
(.env) $ pip install -U pip
(.env) $ pip install -r requirements.txt
```


## Download C2000WARE

* Execute the ***auto_download_c2000ware.py*** and input the ID/Password the TI web page

* Wait a seconds, can check the downloaded file in the **~/Downloads** directory

```bash
(.env) $ ./auto_download_c2000ware.py


##################################################################
TI Login ID: <input your ID>
Password: <input your password>
[Auto Downlaod C2000WARE] Access the web page,,
[Auto Download C2000WARE] Find the Package for Linux.
[Auto Download C2000WARE] Version: 5.00.00.00
[Auto Download C2000WARE] Try to Login,,
[Auto Download C2000WARE] Check the Goverment Approval.
[Auto Download C2000WARE] Start the download the package!!
[Auto Download C2000WARE] Downloading,, 362.53MB
[Auto Download C2000WARE] Complete! 366.68MB
##################################################################


```
