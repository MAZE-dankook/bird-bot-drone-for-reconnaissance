#!/usr/bin/env python3
""" basic crawling for c2000ware """

import os
import time

from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait

from enum_class import ClickMode


###############################################################################
#  Crawling class
class Crawling():
    """ basic crawling class """
    C2000_WARE_FOR_LINUX_URL = "https://www.ti.com/tool/C2000WARE"

    def __init__(self) -> None:
        self.__user_agent = self.__get_user_agent()
        self.__options = ChromeOptions()
        self.__set_chrome_options()

        self.__driver = webdriver.Chrome(options=self.__options)
        self.__driver.get(Crawling.C2000_WARE_FOR_LINUX_URL)

    def __del__(self) -> None:
        if self.__driver:
            self.__driver.quit()

    ###########################################################################
    # Public methods
    def input_data(self, selectors: str, data: str) -> bool:
        """ input data to brower """
        try:
            button = self.__driver.find_element(By.CSS_SELECTOR, selectors)
            button.send_keys(data)
            WebDriverWait(self.__driver, 10)
            time.sleep(0.1)

            return True
        except WebDriverException:
            print("[ERR] Failed to input data in the web." +
                  f"css selector: {selectors}")
            return False

    def click(self, selectors: list, mode: int) -> bool:
        """ click event """
        try:
            css = selectors[0]
            element = self.__driver.find_element(By.CSS_SELECTOR, css)

            if mode == ClickMode.SINGLE_SHADOW_ROOT:
                element = self.__click_single_shadow_root(
                    element, selectors[1])
            elif mode == ClickMode.MULTIPLE_SHADOW_ROOT:
                element = self.__click_multiple_shadow_root(element, selectors)

            if mode == ClickMode.CHECKBOX:
                element.click()
            else:
                element.send_keys(Keys.ENTER)

            WebDriverWait(self.__driver, 10)
            time.sleep(0.5)

            return True
        except WebDriverException:
            print("[ERR] Failed to click event in the web." +
                  f"css selector: {selectors}")
            return False

    def login(self, user_id: str, user_passwd: str) -> bool:
        """ login """
        try:
            if self.input_data("#username", user_id) is False:
                return False

            if self.click(["#nextbutton", "button"],
                          ClickMode.SINGLE_SHADOW_ROOT) is False:
                return False

            if self.input_data("#password > input[type=password]",
                               user_passwd) is False:
                return False

            if self.click(["#loginbutton", "button"],
                          ClickMode.SINGLE_SHADOW_ROOT) is False:
                return False

            return True
        except WebDriverException:
            print("[ERR] Failed to login in the web.")
            return False

    def get_info(self, selectors: str, tag: str) -> str:
        """ return the information """
        element = self.__driver.find_element(By.CSS_SELECTOR, selectors)
        return element.get_attribute(tag)

    ###########################################################################
    # Internal methods
    def __set_chrome_options(self) -> None:
        self.__options.add_argument(f"user-agent={self.__user_agent}")
        self.__options.add_argument("lang=ko_KR")
        self.__options.add_argument("headless")
        self.__options.add_argument("window-size=1920x1080")
        self.__options.add_argument("disable-gpu")
        self.__options.add_argument("--no-sandbox")
        self.__options.add_argument("start-maximized")
        self.__options.add_argument("--disable-extensions")
        self.__options.add_argument(
            "--safebrowsing-disable-download-protection")
        self.__options.add_argument("safebrowsing-disable-extension-blacklist")
        self.__options.add_experimental_option(
            "prefs", {
                "download.default_directory":
                os.path.expanduser("~/Downloads"),
                'download.prompt_for_download': False,
                'download.extensions_to_open': 'xml',
                'safebrowsing.enabled': True
            })

    def __get_user_agent(self) -> str:
        self.__driver = webdriver.Chrome()
        ret = self.__driver.execute_script("return navigator.userAgent")
        self.__driver.quit()

        return ret

    def __get_shadow_root(self, element: WebElement,
                          selector: str) -> WebElement:
        shadown_root = self.__driver.execute_script(
            "return arguments[0].shadowRoot", element)
        return shadown_root.find_element(By.CSS_SELECTOR, selector)

    def __click_single_shadow_root(self, element: WebElement,
                                   selector: str) -> WebElement:
        return self.__get_shadow_root(element, selector)

    def __click_multiple_shadow_root(self, element: WebElement,
                                     selectors: list) -> WebElement:
        css = selectors.pop(0)
        for selector in selectors:
            css += selector
            element = element.find_element(By.CSS_SELECTOR, css)
            WebDriverWait(element, 10)

        return element
