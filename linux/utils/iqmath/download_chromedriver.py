#!/usr/bin/env python3
""" Download Chrome Driver """

import sys
import time

from seleniumbase import Driver


def download_chromedriver() -> None:
    """ Download chrome driver for suitable Chrome version """
    driver = Driver(uc=True)
    driver.get("https://nowsecure.nl/#relax")

    time.sleep(6)

    driver.quit()


if __name__ == "__main__":
    sys.exit(download_chromedriver() or 0)
