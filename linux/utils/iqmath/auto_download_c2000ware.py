#!/usr/bin/env python3
""" Auto download the C2000WARE """

import os
import sys
import time
from glob import glob

import pwinput

from crawling import Crawling
from enum_class import (ClickMode, SelectorAgree, SelectorCivil,
                        SelectorDownload, SelectorLinuxInstaller,
                        SelectorOptions, SelectorPackageDownload,
                        SelectorSubmit, SelectorVersion)


###############################################################################
#  auto download c2000WARE class
class AutoDownloadC2000WARE():
    """ Auto Download C2000WARE Class """
    def __init__(self, user_id: str, user_passwd: str) -> None:
        self.__id = user_id
        self.__passwd = user_passwd

        self.__version = "0"
        self.__ware = Crawling()

    def __del__(self) -> None:
        if self.__ware:
            del self.__ware

    ###########################################################################
    # Public methods
    def run(self) -> None:
        """ run """
        if self.__access_web_page() is False:
            return

        if self.__find_package_for_linux() is False:
            return

        if self.__login() is False:
            return

        if self.__check_approval() is False:
            return

        if self.__start_download_package() is False:
            return

    ###########################################################################
    # Internal methods
    def __access_web_page(self) -> bool:
        print("[Auto Downlaod C2000WARE] Access the web page,,")
        return self.__ware.click(SelectorDownload.list(), ClickMode.NORMAL)

    def __find_package_for_linux(self) -> bool:
        print("[Auto Download C2000WARE] Find the Package for Linux.")
        if self.__ware.click(SelectorOptions.list(),
                             ClickMode.SINGLE_SHADOW_ROOT) is False:
            return False

        version = self.__ware.get_info(SelectorVersion.VERSION, "innerHTML")
        self.__version = version.split(": ")[1]
        print(f"[Auto Download C2000WARE] Version: {self.__version}")

        return self.__ware.click(SelectorLinuxInstaller.list(),
                                 ClickMode.MULTIPLE_SHADOW_ROOT)

    def __login(self) -> bool:
        print("[Auto Download C2000WARE] Try to Login,,")
        return self.__ware.login(self.__id, self.__passwd)

    def __check_approval(self) -> bool:
        print("[Auto Download C2000WARE] Check the Goverment Approval.")
        if self.__ware.click(SelectorCivil.list(),
                             ClickMode.CHECKBOX) is False:
            return False

        if self.__ware.click(SelectorAgree.list(),
                             ClickMode.CHECKBOX) is False:
            return False

        return self.__ware.click(SelectorSubmit.list(), ClickMode.NORMAL)

    def __start_download_package(self) -> bool:
        print("[Auto Download C2000WARE] Start the download the package!!")
        # remove pre packages
        del_lists = glob(os.path.expanduser("~/Downloads/C2000Ware*"))
        for del_list in del_lists:
            os.remove(del_list)

        # click submit button
        self.__ware.click(SelectorPackageDownload.list(), ClickMode.NORMAL)

        # wait to start the download
        while True:
            time.sleep(1)
            if "crdownload" in get_latest_download_file():
                break
            print("[Auto Download C2000WARE] Wait a reponse from TI.")

        # file check
        major, minor1, minor2, minor3 = self.__version.split(".")
        file_name = f"C2000Ware_{major}_{minor1}_{minor2}_{minor3}_setup.run"
        file_name += ".crdownload"
        real_file_path = os.path.expanduser(f"~/Downloads/{file_name}")
        if not os.path.exists(real_file_path):
            print(f"[ERR] Not exist the file {file_name}")
            return False

        # downloading
        while True:
            downloading = get_latest_download_file()
            if "crdownload" not in downloading:
                break

            time.sleep(1)

            try:
                size = os.path.getsize(real_file_path) / (1024 * 1024)
                print(f"[Auto Download C2000WARE] Downloading,, {size:.2f}MB",
                      end="\r")
            except FileNotFoundError:
                print("", end="\n")
                break

        idx = real_file_path.rfind(".")
        download_file = real_file_path[:idx]
        size = os.path.getsize(download_file) / (1024 * 1024)
        print(f"[Auto Download C2000WARE] Complete! {size:.2f}MB")
        return True


def get_latest_download_file() -> str:
    """ return the latest file for checking the download file """
    os.chdir(os.path.expanduser("~/Downloads"))
    files = sorted(os.listdir(os.getcwd()), key=os.path.getmtime)
    return files[-1]


def main() -> None:
    """ main """
    print("\n\n")
    print("##################################################################")
    user_id = input("TI Login ID: ")
    user_passwd = pwinput.pwinput()

    ware = AutoDownloadC2000WARE(user_id, user_passwd)
    ware.run()
    print("##################################################################")
    print("\n\n")


if __name__ == "__main__":
    sys.exit(main() or 0)
