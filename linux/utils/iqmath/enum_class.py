""" enum class """

from enum import Enum


###############################################################################
#  enum class
class ClickMode(Enum):
    """ click mode """
    NORMAL = 0
    SINGLE_SHADOW_ROOT = 1
    MULTIPLE_SHADOW_ROOT = 2
    CHECKBOX = 3


class EnumList(Enum):
    """ for create the list using enum values """
    @classmethod
    def list(cls):
        """ create to list """
        return [content.value for content in cls if "SECT" not in str(content)]


class SelectorDownload(str, EnumList):
    """ Download """
    SECT_1 = "body > main > section:nth-child(1) > div > div.ti_p-row > div."
    SECT_2 = "ti_p-col-3.ti_p-col-phone-12.u-text-right.u-text-left-only-on-"
    SECT_3 = "phone > ti-button > a"

    DOWNLOAD = f"{SECT_1}{SECT_2}{SECT_3}"


class SelectorOptions(str, EnumList):
    """ Options """
    SECT_1 = "#asset-item-MD-xwaqzaqOcB > div.ti_p-col-2.ti_p-col-phone-12."
    SECT_2 = "mod-row-space-phone > ti-button.u-fullWidth.ga-track.u-margin-"
    SECT_3 = "top-2.ti-button-compact.ti-button-primary.hydrated"

    OPTIONS_ONE = f"{SECT_1}{SECT_2}{SECT_3}"
    OPTIONS_TWO = "button"


class SelectorVersion(str, EnumList):
    """ Download Version(latest) """
    SECT_1 = "#asset-item-download-options-card-MD-xwaqzaqOcB > ti-card > div "
    SECT_2 = "> div.u-download-options-nav.u-margin-bottom-6 > div.u-flex."
    SECT_3 = "mod-column-only-on-phone.u-margin-bottom-6 > div.u-flex.mod-"
    SECT_4 = "align-center.u-padding-bottom-2.u-margin-right-4 > span"

    VERSION = f"{SECT_1}{SECT_2}{SECT_3}{SECT_4}"


class SelectorLinuxInstaller(str, EnumList):
    """ Linux Installer """
    SECT_1 = " > section > div.u-divide-y.mod-no-top-line.u-margin-bottom-6 > "
    SECT_2 = "div:nth-child(2) > div > div.u-margin-right-8 > a"

    INSTALLER_ONE = "#asset-item-download-options-card-MD-xwaqzaqOcB > ti-card"
    INSTALLER_TWO = " > div > div:nth-child(3) > ti-tab-container"
    INSTALLER_THREE = " > ti-tab-panel:nth-child(1)"
    INSTALLER_FOUR = f"{SECT_1}{SECT_2}"


class SelectorCivil(str, EnumList):
    """ Checkbox for the Civil """
    CIVIL = "#Civil"


class SelectorAgree(str, EnumList):
    """ radio button for the agree the policy """
    AGREE = "#radio"


class SelectorSubmit(str, EnumList):
    """ submit for approval """
    SECT_1 = "#certifyAllDiv > table > tbody > tr:nth-child(3) > td > table > "
    SECT_2 = "tbody > tr > td > input"

    SUBMIT = f"{SECT_1}{SECT_2}"


class SelectorPackageDownload(str, EnumList):
    """ final click for the download the package of the C2000WARE """
    DOWNLOAD = "#Download"
