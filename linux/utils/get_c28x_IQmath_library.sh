#!/bin/bash
# shellcheck disable=SC1090,SC1091

ROOT_DIR=$(git rev-parse --show-toplevel)
COMMON_DIR="${ROOT_DIR}/scm/scripts/common"

LINUX_DIR="${ROOT_DIR}/linux"
DOWNLOAD_DIR="${HOME}/Downloads"
IQ_DIR="libraries/math/IQmath"

WARE="C2000Ware"

function auto_download_ware()
{
	DOWNLOADED_FILE=$(find "${DOWNLOAD_DIR}" -name "${WARE}_*.run" | tail -n 1)
	if [[ -f "${DOWNLOADED_FILE}" ]]
	then
		echo_func "[C28x IQ] Already downloaded C2000WARE,," 1
		return
	fi

	echo_func "[C28x IQ] setup the virtual envrionment" 0
	cd "${LINUX_DIR}/utils/iqmath"

	# active virtual envrionment
	if [[ ! -d ".env" ]]
	then
		python3 -m venv .env
	fi
	source ".env/bin/activate"

	# install the packages
	echo_func "[C28x IQ] Install the Python packages" 0
	pip install -U pip
	pip install -r requirements.txt

	echo_func "[C28x IQ] Install the Chrome Driver" 0
	python download_chromedriver.py

	# download
	echo_func "[C28x IQ] Try to download C2000WARE" 0
	python auto_download_c2000ware.py

	# deactive virtual envrionment
	deactivate
	cd -
}

function prerequisite()
{
	cd "${DOWNLOAD_DIR}"

	# check to exist the install file
	WARE_INSTALL=$(find . -name "${WARE}_*.run" | tail -n 1)
	if [[ ! -f "${WARE_INSTALL}" ]]
	then
		echo_func "[ERR] Not exist the ${WARE_INSTALL},," 1
		exit 1
	fi

	# get version and set uninstaller
	if [[ "${WARE_INSTALL:0:2}" == "./" ]]
	then
		WARE_INSTALL="${WARE_INSTALL: 2}"
	fi
	MATCH="${WARE}_(.*)_setup.run"
	[[ "${WARE_INSTALL}" =~ ${MATCH} ]]
	VERSION="${BASH_REMATCH[1]}"
	WARE_UNINSTALL="${WARE}_${VERSION}_uninstaller"

	echo_func "[C28x IQ] Setup file is ${WARE_INSTALL}" 0

	# set the execution permission
	if [[ ! -x "${WARE_INSTALL}" ]]
	then
		chmod +x "${WARE_INSTALL}"
	fi

	cd -
}

set -e

source "${COMMON_DIR}/echo.sh"

# check the installed directory
if [[ -d "${LINUX_DIR}/IQmath" ]]
then
	echo_func "[C28x IQ] Exist the IQmath already." 1
	exit 0
fi

# download ware
auto_download_ware

# set up the variables
prerequisite

# install
set +e
echo_func "[C28x IQ] Try to install,," 0
"${DOWNLOAD_DIR}/${WARE_INSTALL}" \
	--mode unattended \
	--unattendedmodeui minimal \
	--prefix "${LINUX_DIR}"
set -e

# expose the IQmath directory
echo_func "[C28x IQ] Expose the \"IQmath\" directory,," 0
cp -rf "${LINUX_DIR}/${WARE}_${VERSION}/${IQ_DIR}" "${LINUX_DIR}"
sync

# uninstall
"${LINUX_DIR}/${WARE}_${VERSION}/uninstallers/${WARE_UNINSTALL}" \
	--mode unattended
rm -rf "${LINUX_DIR}/${WARE}_${VERSION}"

echo_func "[C28x IQ] Done!" 0
