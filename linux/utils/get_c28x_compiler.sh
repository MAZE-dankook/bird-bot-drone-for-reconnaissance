#!/bin/bash

ROOT_DIR=$(git rev-parse --show-toplevel)
COMMON_DIR="${ROOT_DIR}/scm/scripts/common"

LINUX_DIR="${ROOT_DIR}/linux"
CCS_DIR="${LINUX_DIR}/ccs"
COMPILER_DIR="${CCS_DIR}/ccs/tools/compiler/ti-cgt-c2000_22.6.0.LTS"
CGTOOLS="${LINUX_DIR}/cgtools"

MAJOR_VER="12.2.0"
MINOR_VER="00009"

CCS_IDE="CCS${MAJOR_VER}.${MINOR_VER}_linux-x64"
CCS_TAR="${CCS_IDE}.tar.gz"
CCS_IDE_INSTALL="ccs_setup_${MAJOR_VER}.${MINOR_VER}.run"
CCS_IDE_UNINSTALL="uninstall_ccs.run"

LINK_TI_SW="https://dr-download.ti.com/software-development"
LINK_SUB="ide-configuration-compiler-or-debugger/MD-J1VdearkvK"
DOWNLOAD_LINK="${LINK_TI_SW}/${LINK_SUB}/${MAJOR_VER}/${CCS_TAR}"

function delete_ccs()
{
	cd "${LINUX_DIR}"

	if [[ -d "${CCS_DIR}" ]]
	then
		"${CCS_DIR}/ccs/${CCS_IDE_UNINSTALL}" --debuglevel 4 --mode unattended
	fi

	if [[ -d "${CCS_IDE}" ]]
	then
		rm -rf "${CCS_IDE}"
	fi

	if [[ -f "${CCS_TAR}" ]]
	then
		rm "${CCS_TAR}"
	fi

	cd -
}

set -e

source "${COMMON_DIR}/echo.sh"

# check the compiler
if [[ -d "${CGTOOLS}" && -f "${CGTOOLS}/bin/cl2000" ]]
then
	echo_func "[C28x CC] Exist the compiler already." 1
	exit 0
fi

# clean
delete_ccs
cd "${LINUX_DIR}"
if [[ -d "${CGTOOLS}" ]]
then
	rm -rf "${CGTOOLS}"
fi

# Download CCS from TI
echo_func "[C28x CC] Try to download ${CCS_TAR},," 0
wget ${DOWNLOAD_LINK}
if [[ ! -f "${CCS_TAR}" ]]
then
	echo_func "[ERR] Failed to download the CCS IDE." 1
	exit 1
fi

# Unzip CCS IDE and install the compiler
tar -xvf "${CCS_TAR}"

# Install the Compiler
echo_func "[C28x CC] Try to install,," 0
"${CCS_IDE}/${CCS_IDE_INSTALL}" \
	--debuglevel 4 \
	--mode unattended \
	--unattendedmodeui minimal \
	--enable-components PF_C28 \
	--prefix "${CCS_DIR}"

if [[ ! -d "${COMPILER_DIR}" ]]
then
	echo_func "[ERR] Failed to create the compiler for the c28x." 1
	delete_ccs
	exit 1
fi

# create the cgtools directory and copy the compiler
echo_func "[C28x CC] Expose the \"cgtools\" directory,," 0
mkdir -p "${CGTOOLS}"
cd "${COMPILER_DIR}" && cp -rf ./* "${CGTOOLS}" && cd -
sync

# clean
delete_ccs

echo_func "[C28x CC] Done!" 0
