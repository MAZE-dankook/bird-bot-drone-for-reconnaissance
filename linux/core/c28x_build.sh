#!/bin/bash

GIT_DIR=$(git rev-parse --show-toplevel)
COMMON_DIR="${GIT_DIR}/scm/scripts/common"

export ROOT_DIR="${GIT_DIR}/linux"
export CORE_DIR="${ROOT_DIR}/core"
export CGTOOL_DIR="${ROOT_DIR}/cgtools"
export IQ_DIR="${ROOT_DIR}/IQmath/c28"
export C280x_DIR="${CORE_DIR}/280x"

export TARGET="a_c28x_for_linux"

OUTPUT_DIR="${CORE_DIR}/make_build"

function check_output()
{
	if [[ ! -e "${1}" ]]
	then
		echo_func "[ERR] Failed to get the ${1},," 1
		exit 1
	fi
}

function check_needed_directory()
{
	if [[ ! -d ${1} ]]
	then
		echo_func "[ERR] Not exist the ${1} directory,," 1
		echo_func "[ERR] Execution the script in the ${ROOT_DIR}/utils first!" 1
		exit 1
	fi
}

set -e

source "${COMMON_DIR}/echo.sh"

# check necessary directory
check_needed_directory "${CGTOOL_DIR}"
check_needed_directory "${IQ_DIR}"
if [[ ! -d "${OUTPUT_DIR}" ]]
then
	mkdir -p "${OUTPUT_DIR}"
fi

# clean
echo_func "[C28x Build] Clean the project." 0
make clean

# build
echo_func "[C28x Build] Try to the build project." 0
make
check_output "${OUTPUT_DIR}/${TARGET}.out"

# expose the hex file from COFF
echo
echo_func "[C28x Build] Expose the \"hex\" file." 0
echo
cd "${OUTPUT_DIR}"
"${CGTOOL_DIR}/bin/hex2000" \
	"${TARGET}.out" \
	--intel \
	--outfile="${TARGET}.hex" \
	--map="${TARGET}.map" \
	--memwidth=16 \
	--romwidth=16
check_output "${TARGET}.hex"

cd -
echo
echo_func "[C28x Build] Done!" 0
