/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:ffs=unix */
#ifndef VFD_H
#define VFD_H

void vfd_init(void);
void vfd_printf(char *form, ...);

#endif /* VFD_H */
