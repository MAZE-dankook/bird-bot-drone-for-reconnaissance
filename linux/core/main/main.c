#define EXTERNAL_VARIABLE

#include <280x/DSP280x_Device.h>   // DSP281x Headerfile Include File
#include <280x/DSP280x_Examples.h> // DSP281x Examples Include File

static void init_system(void);
static void init_variables(void);

static void init_system(void)
{
    DINT;
    InitSysCtrl();
    InitGpio();
    InitCpuTimers();

    MemCopy(&RamfuncsLoadStart, &RamfuncsLoadEnd, &RamfuncsRunStart);
    MemCopy(&RamfuncsLoadStart1, &RamfuncsLoadEnd1, &RamfuncsRunStart1);

    InitSci();
    InitSpi();
    InitPieCtrl();
    IER = 0x0000;
    IFR = 0x0000;
    InitPieVectTable();
    InitAdc();

    Init_ISR();

    InitEPWM(&LeftPwmRegs);
    InitEPWM(&RightPwmRegs);

    InitEQep(&LeftQepRegs);
    InitEQep(&RightQepRegs);
}

static void init_variables(void) { vfd_init(); }

void Delay(Uint32 Cnt)
{
    while (Cnt--)
    {
        asm("		nop");
        asm("	nop");
    }
}

int main(void)
{
    init_system();
    init_variables();

    // LOAD

    vfd_printf("Linux28x");

    while (1)
    {
    }

    return 0;
}
