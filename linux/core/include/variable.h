/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:ffs=unix */
#ifndef VARIABLE_H
#define VARIABLE_H

#ifdef EXTERNAL_VARIABLE
#undef VARIABLE_EXT
#define VARIABLE_EXT
#else
#define VARIABLE_EXT extern
#endif

#endif /* VARIABLE_H */
