# Build Environment for the TMS320F28x in the Linux


## Directory Tree

* Can show when executing command below,

```bash
$ tree -I 'cmake_build|make_build'
.
├── 280x
│   ├── DSP280x_Adc.c
│   ├── DSP280x_CodeStartBranch.asm
│   ├── DSP280x_CpuTimers.c
│   ├── DSP280x_DefaultIsr.c
│   ├── DSP280x_EPwm.c
│   ├── DSP280x_EQep.c
│   ├── DSP280x_GlobalVariableDefs.c
│   ├── DSP280x_Gpio.c
│   ├── DSP280x_Headers_nonBIOS.cmd
│   ├── DSP280x_MemCopy.c
│   ├── DSP280x_PieCtrl.c
│   ├── DSP280x_PieVect.c
│   ├── DSP280x_Sci.c
│   ├── DSP280x_Spi.c
│   ├── DSP280x_SysCtrl.c
│   ├── DSP280x_usDelay.asm
│   ├── F2808.cmd
│   └── Makefile
├── c28x_build.sh
├── CMakeLists.txt
├── include
│   ├── 280x
│   │   ├── DSP280x_Adc.h
│   │   ├── DSP280x_CpuTimers.h
│   │   ├── DSP280x_DefaultIsr.h
│   │   ├── DSP280x_DevEmu.h
│   │   ├── DSP280x_Device.h
│   │   ├── DSP280x_ECan.h
│   │   ├── DSP280x_ECap.h
│   │   ├── DSP280x_EPwm.h
│   │   ├── DSP280x_EQep.h
│   │   ├── DSP280x_Examples.h
│   │   ├── DSP280x_GlobalPrototypes.h
│   │   ├── DSP280x_Gpio.h
│   │   ├── DSP280x_I2c.h
│   │   ├── DSP280x_PieCtrl.h
│   │   ├── DSP280x_PieVect.h
│   │   ├── DSP280x_Sci.h
│   │   ├── DSP280x_Spi.h
│   │   ├── DSP280x_SWPrioritizedIsrLevels.h
│   │   ├── DSP280x_SysCtrl.h
│   │   └── DSP280x_XIntrupt.h
│   └── variable.h
├── main
│   ├── main.c
│   ├── main.h
│   └── Makefile
├── Makefile
├── motor
│   ├── Makefile
│   ├── motor.c
│   └── motor.h
├── README.md
├── sensor
│   ├── Makefile
│   ├── sensor.c
│   └── sensor.h
└── vfd
    ├── Makefile
    ├── vfd.c
    └── vfd.h
```


### 280x

* This directory is for ***DSP280x_**** C codes, headers, cmd files and basic asm files for the C280x.


### include

* This directory has basic ***header*** files for the global codes.


### main

* This directory has the starting point of the program and while loop for continuous execution of the Firmware.


### motor and sensor

* Basic codes for the motor and sensor, but currently that codes are empty.


### vfd

* The basic firmware codes were uploaded for the digit dot matrix led display(***HCMS-29x***).


### Makefile

* Set the compiler/linker path, directory for build and cmd files.

* Used the C compile option is refer to the doc below,
  * [TMS320C28x Optimizing C/C++ Compiler](https://gitlab.com/MAZE-dankook/bird-bot-drone-for-reconnaissance/-/wikis/uploads/e91a83971f0b65763e4b8b2b322e4e86/TMS320C28x_Optimizing_C_C++_Compiler_v22.6.0.LTS.pdf)

* Used the Linker option is refer to the doc below,

  * [TMS320C28x Assembly Language Tools](https://gitlab.com/MAZE-dankook/bird-bot-drone-for-reconnaissance/-/wikis/uploads/ce83133f45870e53b0be6573b4599fc8/TMS320C28x_Assembly_Language_Tools_v21.6.0.LTS_User_s_Guide.pdf)

* The output file is in COFF format(*.out) only.


### Build Script(***c28x_build.sh***)

* The script checks for the necessary directory to build and tries to clean-build.

* The output file is hex format from the COFF.


## How to build

* First, you need to create the ***make_build, cmake_build*** directory for checking build results,

```bash
$ mkdir -p make_build cmake_build
```


### Using Makefile directly

* Try to build as the command below,

```bash
$ make
```

* Try to clean as the command below,

```bash
$ make clean
```

* After the build, you can see the ****.out*** file in the **make_build** directory.


### Using Bash script(***c28x_build.sh***)

```bash
$ ./c28x_build.sh
```

* After the build, you can see the ****.hex*** file in the **make_build** directory.


### Using CMake(***CMakeLists.txt***)

* First, create the Makefile through cmake

```bash
$ cd cmake_build
$ cmake ../
```

* After executed that command, you can see the created the Makefile

```bash
$ make VERBOSE=1
```

* After the build, you can see the ****.out*** file in the **output** directory.

```bash
$ ls -al ./output
```

* Try to clean as the command below,

```bash
$ make clean
$ ls -al ./output
```

* Try to distclean as the command below,

```bash
$ cd ../
$ rm -rf cmake_build/*
```
